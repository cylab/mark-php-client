<?php

namespace Cylab\Mark;

class JsonRPCTest extends \PHPUnit\Framework\TestCase
{
    public function testHello()
    {
        $c = new \JsonRPC\Client("http://127.0.0.1:8080");
        $this->assertEquals("1", $c->execute("test"));
    }
}
