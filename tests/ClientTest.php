<?php

namespace Cylab\Mark;

class ClientTest extends \PHPUnit\Framework\TestCase
{

    private $client;

    private function client() : Client
    {
        if ($this->client !== null) {
            return $this->client;
        }

        $this->client = new Client();
        return $this->client;
    }

    public function testVersion()
    {
        $this->assertEquals("1", $this->client()->test());
        $this->assertEquals("2.6.0", $this->client()->status()["version"]);
    }

    public function testActivation()
    {

        $this->assertEquals(
            "detection.max.1h",
            $this->client()->activation()[0]->label
        );

        $this->assertEquals(
            "https://javadoc.io/doc/be.cylab.mark/server/latest/be/cylab/mark/detection/Max.html",
            $this->client()->activation()[0]->javadoc()
        );
    }

    /**
     * @group sources
     */
    public function testSources()
    {
        $this->assertEquals(
            "be.cylab.mark.example.ExampleDataSource",
            $this->client()->sources()[0]->class_name
        );
    }

    public function testPauseResume()
    {

        $this->client()->pause();
        $this->assertEquals(false, $this->client()->status()["running"]);

        $this->client()->resume();
        $this->assertEquals(true, $this->client()->status()["running"]);

        $this->client()->reload();
        $this->assertEquals(true, $this->client()->status()["running"]);
    }

    public function testFindEvidence()
    {
        $evidences = $this->client()->findEvidence("detection.timeaverage");
        $ev1 = $evidences[0];

        //var_dump($ev1->timeForHumans());
        $this->assertIsString($ev1->timeForHumans());

        $this->assertEquals(
            $ev1->score,
            $this->client()->findEvidenceById($ev1->id)->score
        );

        $evidences = $this->client()->findEvidenceSince(
            "detection.timeaverage",
            ["name" => "Tibo"],
            (time() - 100) * 1000
        );

        $this->assertIsArray($evidences);
        $this->assertTrue(count($evidences) > 0);
        $this->assertEquals(
            "detection.timeaverage",
            $evidences[0]->label
        );
    }


    public function testFindData()
    {

        $q = '{"id":"1401159775","jsonrpc":"2.0","method":"findRawData",'
                . '"params":["data.dummy",{"name":"Tibo"},0,'
                . time() * 1000 . ']}';
        $this->assertTrue(count($this->client()->findDataByQuery($q)) > 0);
    }

    public function testFindLast()
    {

        $this->assertEquals(100, count($this->client()->findLastData()));
        $this->assertEquals(100, count($this->client()->findLastEvidences()));
    }

    public function testAddData()
    {
        $label = "testAddData";
        $subject = ["name" => "Garfield"];

        $record = new Record();
        $record->subject = $subject;
        $record->label = $label;
        $record->time = time() * 1000;
        $record->data = "some data regarding subject...";
        $this->client()->addData($record);

        $from = (time() - 10) * 1000;
        $till = (time() + 1) * 1000;


        $records = $this->client()->findData($label, $subject, $from, $till);
        $this->assertIsArray($records);
        $this->assertTrue(count($records) > 0);
        $this->assertEquals($label, $records[0]->label);
        $this->assertEquals($subject, $records[0]->subject);
    }
}
