<?php

// detector-parameters.php
// change the paramters of a detector

require __DIR__ . "/../vendor/autoload.php";

use Cylab\Mark\Client;

$label = "detection.max.1h";

$client = new Client();
$detector = $client->getDetector($label);

// modify the parameters of this detector
$detector->parameters["window"] = random_int(123, 123123);
$client->setDetector($detector);
