<?php

namespace Cylab\Mark;

/**
 * Description of Client
 *
 * @author tibo
 */
class Client
{

    private $client;

    public function __construct(string $url = "http://127.0.0.1:8080")
    {
        $this->client = new \JsonRPC\Client($url);
    }

    /**
     *
     * @return string
     */
    public function test() : string
    {
        return $this->client->execute("test");
    }

    /**
     * Query the status of the server.
     *
     * Returns an array with fields like:
     * Array
     * (
     *     [db.evidence.count] => 0
     *     [executor.jobs.executed] => 0
     *     [executor.jobs.waiting] => 0
     *     [memory.total] => 3948
     *     [db.data.count] => 0
     *     [processors] => 8
     *     [version] => 1.3.1
     *     [db.evidence.size] => 0
     *     [os.version] => 5.0.0-32-generic
     *     [running] => 1
     *     [db.data.size] => 0
     *     [load] => 1.11
     *     [memory.used] => 470
     *     [executor.parallelism] => 10
     *     [os.name] => Linux
     *     [executor.nodes] => 1
     *     [arch] => amd64
     *     [executor.jobs.running] => 0
     * )
     * @return array
     */
    public function status() : array
    {
        return $this->client->execute("status");
    }

    /**
     * Get the list of configured detectors.
     *
     * Return an array of detectors
     * @return array(Detector)
     */
    public function activation() : array
    {
        $detectors = [];
        foreach ($this->client->execute("activation") as $values) {
            $detectors[] = new Detector($values);
        }
        return $detectors;
    }

    /**
     *
     * @param string $label
     * @return \Cylab\Mark\Detector;
     * @throws \Exception
     */
    public function getDetector(string $label)
    {
        foreach ($this->activation() as $detector) {
            /** @var Detector $detector */
            if ($detector->label == $label) {
                return $detector;
            }
        }

        throw new \Exception("Invalid label: " . $label);
    }



    /**
     * Get the configuration of the data sources.
     *
     * @return array
     */
    public function sources() : array
    {
        $sources = [];
        foreach ($this->client->execute("sources") as $values) {
            $sources[] = new Source($values);
        }
        return $sources;
    }

    /**
     * Get historical status of the server.
     *
     * Returns an array of array:
     * Array
     *    (
     *        [db_evidence_size] => 0
     *        [os_version] => 5.0.0-32-generic
     *        [executor_parallelism] => 10
     *        [executor_nodes] => 1
     *        [executor_jobs_running] => 0
     *        [memory_used] => 473
     *        [processors] => 8
     *        [db_evidence_count] => 0
     *        [version] => 1.3.1
     *        [db_data_count] => 0
     *        [running] => 1
     *        [load] => 1.08
     *        [executor_jobs_waiting] => 0
     *        [os_name] => Linux
     *        [executor_jobs_executed] => 0
     *        [arch] => amd64
     *        [time] => 1601725303894
     *        [memory_total] => 3948
     *        [db_data_size] => 0
     *    )
     * @return array
     */
    public function history() : array
    {
        return $this->client->execute("history");
    }

    /**
     * Pause execution.
     */
    public function pause()
    {
        $this->client->execute("pause");
    }

    /**
     * Resume execution.
     */
    public function resume()
    {
        $this->client->execute("resume");
    }

    /**
     * Reload the configuration of the detectors.
     */
    public function reload()
    {
        $this->client->execute("reload");
    }

    /**
     * Dangerous! Restart the server: wipe the database and restart the data sources.
     */
    public function restart()
    {
        $this->client->execute("restart");
    }


    /**
     * For each subject, find the latest evidence corresponding to this label.
     *
     * Return an array of Evidence
     *
     * @param string $label
     * @return array(Evidence)
     */
    public function findEvidence(string $label) : array
    {
        $array = $this->client->execute("findEvidence", [$label]);
        $evidences = [];
        foreach ($array as $ev) {
            $evidences[] = new Evidence($ev);
        }
        return $evidences;
    }

    /**
     * Find a single evidence by id.
     *
     * @param string $id
     * @return Evidence
     */
    public function findEvidenceById(string $id) : Evidence
    {
        return new Evidence($this->client->execute("findEvidenceById", [$id]));
    }

    /**
     * Get previous reports produced by this detector for this subject.
     *
     * Used to display historical data of this subject.
     *
     * @param string $label
     * @param array $subject
     * @param int $time unix timestamp in milliseconds
     */
    public function findEvidenceSince(string $label, array $subject, int $time) : array
    {
        return $this->parseEvidences(
            $this->client->execute(
                "findEvidenceSince",
                [$label, $subject, $time]
            )
        );
    }

     /**
     * Get previous reports produced by this detector for this subject.
     *
     * Used to display historical data of this subject.
     *
     * @param int $time unix timestamp in milliseconds
     */
    public function findAllEvidenceSince(int $time) : array
    {
        return $this->parseEvidences(
            $this->client->execute(
                "findEvidenceSince",
                [$time]
            )
        );
    }

    /**
     * Get an array of Evidence arrays arranged in a period depending on a specific interval
     * 
     * @param int period
     * @param int interval
     */
    public function findEvidenceForPeriodAndInterval(int $period, $interval) : array
    {
        $array = $this->client->execute("findEvidenceForPeriodAndInterval", [$period, $interval]);
        $bin_array = [];
        foreach ($array as $arr) {
            if (is_null($arr)) {
                $bin_array[] = NULL;
            } else {
                $bin_array[] = $this->parseEvidences($arr);
            }
        }
        return $bin_array;
    }

    /**
     * Get the last evidences that have been inserted in the DB.
     * @return array(Evidence)
     */
    public function findLastEvidences() : array
    {
        return $this->parseEvidences(
            $this->client->execute("findLastEvidences")
        );
    }

    private function parseEvidences(array $array) : array
    {
        $evidences = [];
        foreach ($array as $ev) {
            $evidences[] = new Evidence($ev);
        }
        return $evidences;
    }

    public function addData(Record $data)
    {
        $this->client->execute("addRawData", [$data]);
    }

    /**
     * Find data using a string json query.
     *
     * @param string $query
     * @return array(Record)
     */
    public function findDataByQuery(string $query) : array
    {
        $req = json_decode($query, true);
        $method = $req["method"];
        $params = $req["params"];

        return $this->parseData(
            $this->client->execute($method, $params)
        );
    }

    /**
     *
     * @param string $label
     * @param array $subject
     * @param int $from
     * @param int $till
     * @return array(Record)
     */
    public function findData(string $label, array $subject, int $from, int $till)
    {
        return $this->parseData(
            $this->client->execute(
                "findRawData",
                [$label, $subject, $from, $till]
            )
        );
    }


    private function parseData(array $array) : array
    {
        $records = [];
        foreach ($array as $a) {
            $records[] = new Record($a);
        }
        return $records;
    }

    /**
     * Get the last data records that have been inserted in the DB.
     * @return array
     */
    public function findLastData() : array
    {
        return $this->parseData($this->client->execute("findLastRawData"));
    }

    /**
     * Add or change the configuration of a detector
     * @param \Cylab\Mark\Detector $detector
     * @return void
     */
    public function setDetector(Detector $detector) : void
    {
        $this->client->execute("setAgentProfile", [$detector->toCamelCase()]);
    }
}
