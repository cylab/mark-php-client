<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Cylab\Mark;

/**
 * Description of Evidence
 *
 * @author tibo
 */
class Evidence extends AbstractRecord
{

    public $score;
    public $report;
    public $references;
    public $requests;
    public $profile;

    public function __construct(array $values)
    {
        parent::__construct($values);
        $this->score = $values["score"];
        $this->report = $values["report"];
        $this->references = $values["references"];
        $this->requests = $values["requests"];
        $this->profile = $values["profile"];
    }
}
