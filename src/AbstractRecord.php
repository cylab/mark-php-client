<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Cylab\Mark;

use Carbon\Carbon;

/**
 * Description of AbstractRecord
 *
 * @author tibo
 */
class AbstractRecord
{

    public $id;
    public $label;
    public $time;
    public $subject;

    public function __construct(?array $values = null)
    {
        if ($values === null) {
            return;
        }

        $this->id = $values["id"];
        $this->label = $values["label"];
        $this->time = $values["time"];
        $this->subject = $values["subject"];
    }

    /**
     * Get a textual representation of the subject.
     *
     * @return string
     */
    public function subject() : string
    {
        return implode(" | ", $this->subject);
    }

    /**
     * Get the record timestamp as a Carbon object.
     *
     * @return Carbon
     */
    public function time() : Carbon
    {
        return Carbon::createFromTimestampMs($this->time);
    }

    /**
     * Get the record timestamp as a human readable string
     * @return string
     */
    public function timeForHumans() : string
    {
        return $this->time()->toIso8601String()
                . " (" . $this->time()->diffForHumans() . ")";
    }
}
