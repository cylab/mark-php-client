<?php

namespace Cylab\Mark;

/**
 * Description of AbstractAgent
 *
 * @author tibo
 */
abstract class AbstractAgent
{

    public $label;

    /**
     *
     * @var array associative array
     */
    public $parameters;
    public $class_name;

    public function __construct(array $values)
    {
        $this->label = $values["label"];
        $this->parameters = $values["parameters"];
        $this->class_name = $values["className"];
    }

    /**
     * The URL where the javadoc help for this detector can be found.
     * Something like
     * https://javadoc.io/doc/be.cylab.mark/server/latest/be/cylab/mark/detection/TimeAverage.html
     *
     * @return string
     */
    public function javadoc() : string
    {
        // be.cylab.mark.detection.TimeAverage
        $class = $this->class_name;

        $url = "https://javadoc.io/doc/be.cylab.mark/server/latest/";
        $url .= str_replace(".", "/", $class);
        $url .= ".html";

        return $url;
    }
}
