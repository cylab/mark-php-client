<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Cylab\Mark;

/**
 * Description of Data
 *
 * @author tibo
 */
class Record extends AbstractRecord
{

    public $data;

    public function __construct(?array $values = null)
    {
        parent::__construct($values);

        if ($values === null) {
            return;
        }
        $this->data = $values["data"];
    }
}
