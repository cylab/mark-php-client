<?php

namespace Cylab\Mark;

/**
 * Represent a detector: trigger_label, label for produced evidences,
 * parameters, algorithm class etc.
 *
 * @author tibo
 */
class Detector extends AbstractAgent
{

    public $trigger_label;
    public $trigger_interval;

    public function __construct(array $values)
    {
        parent::__construct($values);
        $this->trigger_label = $values["triggerLabel"];
        $this->trigger_interval = $values["triggerInterval"];
    }

    /**
     * The java implementation of MARk uses camelCase while we use snake_case.
     * Convert this snake_case object to a camelCase associative array.
     * @return array
     */
    public function toCamelCase() : array
    {
        $a = [];
        foreach (get_object_vars($this) as $property => $value) {
            $camel = str_replace("_", "", ucwords($property, "_"));
            $camel[0] = strtolower($camel[0]);
            $a[$camel] = $value;
        }

        if (count($a["parameters"]) == 0) {
            $a["parameters"] = null;
        }

        return $a;
    }
}
