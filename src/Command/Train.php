<?php

namespace Cylab\Mark\Command;

use Cylab\ROC\ROC as ROCAlgorithm;
use Cylab\ROC\SimpleValue;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Simple training framework for MARk.
 *
 * E.g. <pre>php src/Main.php train detection.max.1h tests/truth.json tests/parameters.json</pre>
 *
 * @author tibo
 */
class Train extends AbstractCommand
{

    protected static $defaultName = 'train';

    private $output;

    protected function configure(): void
    {

        $this
                ->setDescription('Train a detector')
                ->addArgument('label', InputArgument::REQUIRED, 'Label of the detector')
                ->addArgument('truth', InputArgument::REQUIRED, 'Json file containing the ground truth')
                ->addArgument(
                    'parameters',
                    InputArgument::REQUIRED,
                    'Json file containing the desciption of parameters'
                );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->output = $output;

        $client = $this->client();
        $label = $input->getArgument('label');
        $this->output->writeln("Starting optimization for " . $label);

        $this->output->writeln("Get initial detector parameters");
        $detector = $client->getDetector($label);
        $parameters = $detector->parameters;

        $this->output->writeln("Get parameters definitions and initialize optimization algorithm");
        $definitions = json_decode(file_get_contents($input->getArgument('parameters')), true);

        $this->output->writeln("Found definitions for " . count($definitions) . " parameter(s)");
        $algo = new SimpleGuess($definitions, $parameters);

        $this->output->writeln("Get list of true alerts (ground truth)");
        $truth = json_decode(file_get_contents($input->getArgument('truth')), true);
        $output->writeln("Found " . count($truth) . " entries in training data");

        while (true) {
            $this->output->writeln("---------");
            $this->output->writeln("Compute new parameters");
            $parameters = $algo->computeNewParameters();
            print_r($parameters);

            $this->output->writeln("Set new parameters and restart server");
            $detector->parameters = $parameters;
            $client->setDetector($detector);
            $client->restart();

            // wait for server to finish
            $this->waitForServer();

            // get results
            $reports = $this->getRankingFor($label);

            // compute AUC
            $auc = $this->computeAUC($reports, $truth, $label);

            // notify the optimization algorithm
            $algo->notify($auc, $parameters);
        }

        // return Command::SUCCESS;
    }

    private function getRankingFor(string $label)
    {
        $reports = $this->client()->findEvidence($label);
        $this->output->writeln("Found " . count($reports) . " reports");
        if (count($reports) < 2) {
            $this->output->writeln("Not enough data!");
            throw new \Exception("Not enough data!");
        }

        return $reports;
    }

    private function computeAUC(array $reports, array $truth, string $label)
    {
        $this->output->writeln("Compute AUC");
        $values = $this->labelReports($reports, $truth);

        // Compute ROC and AUC
        $roc = ROCAlgorithm::fromValues($values);

        $auc = $roc->getAUC();
        $this->output->writeln("AUC: $auc");

        $roc_path = sys_get_temp_dir() . "/" . $label . "-" . date("Ymd-His") . ".png";
        $roc->saveToPNG($roc_path);
        $this->output->writeln("ROC saved to " . $roc_path);

        return $auc;
    }

    private function labelReports($reports, $truth) : array
    {
        $values = [];
        foreach ($reports as $report) {
            $values[] = new SimpleValue(
                $report->score,
                in_array($report->subject, $truth)
            );
        }
        return $values;
    }

    private function waitForServer() : void
    {
        $this->output->write("Wait for server ");
        $client = $this->client();

        while (true) {
            sleep(10);
            $this->output->write(".");
            $status = $client->status();

            if ($status["sources.running"] == 0
                    && $status["executor.jobs.running"] == 0
                    && $status["executor.jobs.waiting"] == 0) {
                $this->output->writeln("");
                break;
            }
        }
    }
}
