<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Cylab\Mark\Command;

/**
 *
 * @author tibo
 */
interface OptimizationAlgorithm
{
    public function __construct(array $definitions, array $initial_parameters);

    public function computeNewParameters() : array;

    public function notify(float $auc, array $parameters) : void;
}
