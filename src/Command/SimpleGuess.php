<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Cylab\Mark\Command;

/**
 * Description of SimpleGuess
 *
 * @author tibo
 */
class SimpleGuess implements OptimizationAlgorithm
{
    /**
     *
     * @var array
     */
    private $parameters;

    /**
     *
     * @var array
     */
    private $definitions;

    public function __construct(array $definitions, array $initial_parameters)
    {
        $this->parameters = $initial_parameters;
        $this->definitions = $definitions;
    }

    public function computeNewParameters(): array
    {
        $new = $this->parameters;

        foreach ($this->definitions as $param) {
            $name = $param["name"];
            $type = $param["type"];
            $min = $param["min"];
            $max = $param["max"];

            $new[$name] = random_int($min, $max);
        }

        return $new;
    }

    public function notify(float $auc, array $parameters): void
    {
    }
}
