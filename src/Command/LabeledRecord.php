<?php

namespace Cylab\Mark\Command;

use Cylab\ROC\Value;

class LabeledRecord implements Value
{

    private $subject;
    private $score;
    private $is_true_alert;


    public function __construct($subject, float $score, bool $is_true_alert)
    {
        $this->subject = $subject;
        $this->score = $score;
        $this->is_true_alert = $is_true_alert;
    }

    public function getScore() : float
    {
        return $this->score;
    }

    public function isTrueAlert() : bool
    {
        return $this->is_true_alert;
    }

    public function getSubject()
    {
        return implode(" ", $this->subject);
    }
}
