<?php

namespace Cylab\Mark\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Description of Ranking
 *
 * @author tibo
 */
class Ranking extends AbstractCommand
{

    protected static $defaultName = 'ranking';

    protected function configure(): void
    {

        $this
                ->setDescription('Get the ranking created by a detector')
                ->addArgument('label', InputArgument::REQUIRED, 'Label of the detector');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $label = $input->getArgument('label');
        $client = $this->client();
        $reports = $client->findEvidence($label);

        $output->writeln(sprintf("%-10s | %-40s", "Score", "Subject"));
        $output->writeln(sprintf("------------------------------------"));

        foreach ($reports as $report) {
            $output->writeln(sprintf("%.8f | %-40s", $report->score, $report->subject()));
        }

        return Command::SUCCESS;
    }
}
