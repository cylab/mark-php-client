<?php

namespace Cylab\Mark\Command;

use Cylab\Mark\Client;
use Symfony\Component\Console\Command\Command;

/**
 * Description of AbstractCommand
 *
 * @author tibo
 */
class AbstractCommand extends Command
{
    private $client;

    /**
     *
     * @return \Cylab\Mark\Client $client
     */
    protected function client()
    {
        if ($this->client == null) {
            $this->client = new Client();
        }

        return $this->client;
    }
}
