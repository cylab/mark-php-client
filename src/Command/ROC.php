<?php

namespace Cylab\Mark\Command;

use Cylab\ROC\ROC as ROCAlgorithm;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Compute the ROC and AUC using a list of real alerts (ground truth).
 *
 * E.g. <pre>php src/Main.php roc detection.example tests/truth.json</pre>
 *
 * @author tibo
 */
class ROC extends AbstractCommand
{

    protected static $defaultName = 'roc';
    private $output;

    protected function configure(): void
    {

        $this
                ->setDescription('Compute the ROC (accuracy) of a detector')
                ->addArgument('label', InputArgument::REQUIRED, 'Label of the detector')
                ->addArgument('truth', InputArgument::REQUIRED, 'Json file containing the ground truth');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->output = $output;

        // Load the ground truth
        $truth = json_decode(file_get_contents($input->getArgument('truth')), true);
        $output->writeln("Found " . count($truth) . " entries in training data");

        // Get the ranking
        $label = $input->getArgument('label');
        $client = $this->client();
        $reports = $client->findEvidence($label);
        $output->writeln("Found " . count($reports) . " reports");
        if (count($reports) < 2) {
            $output->writeln("Not enough data!");
            return Command::SUCCESS;
        }

        // Label the reports
        $values = $this->labelReports($reports, $truth);

        $this->displayLabeledRecords($values);

        // Compute ROC and AUC
        $roc = ROCAlgorithm::fromValues($values);

        $roc_path = sys_get_temp_dir() . "/" . $label . "-" . date("Ymd-His") . ".png";
        $output->writeln("AUC: " . $roc->getAUC());
        $roc->saveToPNG($roc_path);
        $output->writeln("ROC saved to " . $roc_path);

        return Command::SUCCESS;
    }

    private function labelReports($reports, $truth) : array
    {
        $values = [];
        foreach ($reports as $report) {
            $values[] = new LabeledRecord(
                $report->subject,
                $report->score,
                in_array($report->subject, $truth)
            );
        }
        return $values;
    }

    private function displayLabeledRecords(array $values)
    {
        $output = $this->output;
        $output->writeln(sprintf("%-10s | %-40s | %-10s", "Score", "Subject", "True Alert"));
        $output->writeln(sprintf("--------------------------------------------------------------------"));

        foreach ($values as $report) {
            $output->writeln(sprintf(
                "%.8f | %-40s | %b",
                $report->getScore(),
                $report->getSubject(),
                $report->isTrueAlert()
            ));
        }
    }
}
