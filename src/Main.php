#!/usr/bin/env php
<?php

require __DIR__ . "/../vendor/autoload.php";

use Symfony\Component\Console\Application;

$application = new Application();

$application->add(new Cylab\Mark\Command\Ranking());
$application->add(new Cylab\Mark\Command\ROC());
$application->add(new Cylab\Mark\Command\Train());

$application->run();
