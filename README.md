# MARk PHP Client

A PHP client for the MARK framework

## Local testing

```bash
docker-compose up
```

The docker-compose will also start a web interface at http://127.0.0.1:8000

* Username: ```mark-admin@cylab.be```
* Password: ```change-me!```

You can now 

* run unit tests: ```./vendor/bin/phpunit```
* use the CLI (see below)
* write your own scripts (see examples below)

## CLI

The MARk PHP client is also a small command line tool that allows to query a MARk server

### Ranking

```bash
php src/Main.php ranking detection.example
```

will produce:

```
Score      | Subject                                 
------------------------------------
0.83987058 | Georgi                                  
0.56913326 | Wim                                     
0.45586617 | Paloma                                  
0.42554451 | Fred                                    
0.37306698 | Alex                                    
0.24606320 | Zac                                     
0.19540481 | Tibo  
```

### ROC

## Examples

## Detector parameters

To change the parameters of a detector:

```php
require __DIR__ . "/../vendor/autoload.php";

use Cylab\Mark\Client;

$label = "detection.max.1h";

$client = new Client();
$detector = $client->getDetector($label);

// modify the parameters of this detector
$detector->parameters["window"] = random_int(123, 123123);
$client->setDetector($detector);
```

Each time you run this script, the ```window``` parameter of the detector will be replaced by a random value:

![](examples/detector-parameters.png)
